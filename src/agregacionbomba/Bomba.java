/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agregacionbomba;

/**
 *
 * @author 52667
 */
public class Bomba {
    
    private int numBomba;
    private float capacidad;
    private float contador;
    private Gasolina gasolina;

    public Bomba() {
        this.numBomba = 0;
        this.capacidad = 0.0f;
        this.contador = 0.0f;
        this.gasolina = new Gasolina();
    }
    
    public Bomba(Bomba bomba){
        this.numBomba = bomba.numBomba;
        this.capacidad = bomba.capacidad;
        this.contador = bomba.contador;
        this.gasolina = bomba.gasolina;
        
    }
    
    public Bomba(int numBomba, float capacidad, float contador, Gasolina gasolinera) {
        this.numBomba = numBomba;
        this.capacidad = capacidad;
        this.contador = contador;
        this.gasolina = gasolinera;
    }
    
    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public float getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(float capacidad) {
        this.capacidad = capacidad;
    }

    public float getContador() {
        return contador;
    }

    public void setContador(float contador) {
        this.contador = contador;
    }

    public Gasolina getGasolinera() {
        return gasolina;
    }

    public void setGasolinera(Gasolina gasolinera) {
        this.gasolina = gasolinera;
    }
    
    public float obtenerInventario(){
        
        return this.capacidad - this.contador;
        
       
    }
    
    public boolean realizarVenta(float cantidad){
    boolean exito = false;
    
    if (cantidad <= this.obtenerInventario())exito=true;
    return exito;
    }
    
    public float calcularTotal(){
        
        return this.contador * this.gasolina.getPrecio();
    }
    
    
    
    
}
