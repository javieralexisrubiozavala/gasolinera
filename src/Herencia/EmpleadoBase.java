/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author 52667
 */
public class EmpleadoBase extends Empleado implements Impuesto {
   private float pagoDiario;
   private float diasTrabjados;

    public EmpleadoBase() {
        this.pagoDiario = 0.0f;
        this.diasTrabjados = 0.0f;
    }

    public EmpleadoBase(float pagoDiario, float diasTrabjados, int numEmpleado, String nombre, String puesto, String depto) {
        super(numEmpleado, nombre, puesto, depto);
        this.pagoDiario = pagoDiario;
        this.diasTrabjados = diasTrabjados;
    }

    public float getPagoDiario() {
        return pagoDiario;
    }

    public void setPagoDiario(float pagoDiario) {
        this.pagoDiario = pagoDiario;
    }

    public float getDiasTrabjados() {
        return diasTrabjados;
    }

    public void setDiasTrabjados(float diasTrabjados) {
        this.diasTrabjados = diasTrabjados;
    }
           
           
           
    @Override
    public float calcularPago() {
        return this.diasTrabjados * this.pagoDiario;
    }

    @Override
    public float calcularImpuesto() {
      float impuestos = 0;
      if(this.calcularPago()> 5000) impuestos = this.calcularPago() * .16f;
      return impuestos;
    }
    
}
